

function generateEssayPath(essayGraph) {
  let path = null;
  let starting_node = randomMemberOf(essayGraph.filter(node => node.ending_ids.length > 0));
  while (!path) {
    let random_ending_id = randomMemberOf(starting_node.ending_ids);
    path = dfs(starting_node, random_ending_id, [], essayGraph);
  }
  console.log(path);
  return path;
}

function dfs(current_node, ending_id, past_ids, essayGraph) {
  if (current_node.id === ending_id) return [current_node]; //returning condition
  if (past_ids.includes(current_node.id)) return null; // fail condition: Step on a repeated node
  let randomized_next_ids = permute(current_node.next_ids); // recursive step
  for (let i = 0; i < randomized_next_ids.length; i++) {
    let lowerBranch = dfs(findNodeById(randomized_next_ids[i], essayGraph), ending_id, [current_node.id].concat(past_ids), essayGraph);
    if (lowerBranch != null) return [current_node].concat(lowerBranch);
  }
  return null; // failed recursive step
}

function findNodeById(id, graph) {
  return graph.filter( node => node.id === id)[0];
}

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

function randomMemberOf(arr) {
  let ranIndex = randomInt(arr.length);
  return arr[ranIndex];
}

function permute(arr) {
  var addToRandomPosition = function(arr, elem) { arr.splice(randomInt(arr.length), 0, elem); };
  var permutation = [];
  for (let x of arr) addToRandomPosition(permutation, x);
  return permutation;
}

function generateHTMLFromEssayPath(path) {
  let html = `<a href="about.html" id="about">About</a>`;
  html = html + `<h1>${path[0].title}</h1>` + `<h3> by Anthony Tsang, Jan 2020 </h3>`;
  for (let node of path) html = html + `<p>${node.content}</p>`;
  html = html + `<h2> Citations </h2>`;
  for (let reference of generateReferencesFromEssayPath(path)) html = html + `<p>${reference}</p>`;
  return html;
}

function generateReferencesFromEssayPath(path) {
  let referenceList = [];
  for (let node of path) referenceList = referenceList.concat(node.reference);
  return nub(referenceList).sort();
}

function nub(arr) {
  var no_dups = [];
  //remove whitespace so that it is possible to find duplicates regardless of spacing
  for (let x of arr) if (!no_dups.map(y => y.replace(/ /g, "")).includes(x.replace(/ /g, ""))) no_dups.push(x);
  return no_dups;
}

function generateBibliography(essayGraph) {
  var html = ``;
  var refList = [];
  essayGraph.map(node => node.reference.map(r => refList.push(r)));
  refList = nub(refList).sort();
  for (let ref of refList) html = html + `<p>${ref}</p>`;
  return html;
}
